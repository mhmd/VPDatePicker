package com.vp.datepicker;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        VPDatePickerDialog.Builder builder = new VPDatePickerDialog.Builder()
                .setAccentColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        Toast.makeText(MainActivity.this, day + "/" + month + "/" + year,
                                Toast.LENGTH_LONG).show();
                    }
                });
        builder.create().show(fragmentManager, "date_picker");
    }
}
